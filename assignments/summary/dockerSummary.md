# **DOCKER**
Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels.  
### **DOCKER LEXICON**    
#### 1.Docker Container  
Docker container is a block where image is saved. Many container can be created using single image.  
#### 2.Docker Image    
The image can be employed to any Docker enviornment as container.  
* Code  
* Runtime  
* Enviornment  
* Libraries  
* Configuration Files  
#### 3. DockerHub  
It's like Github of Docker. Dockerhub stores images and containers  

### **COMPONENTS OF DOCKER**
![Components](https://static.packt-cdn.com/products/9781788992329/graphics/98f26def-8b4d-49f1-82ae-3341fa152e2e.png)
#### Docker Engine:
The place where containers and runs the programs.
#### Docker Client:
It is the end user which provides the demands of the docker file as commands.
#### Docker daemon:
This place checks the client request and communicate with docker components such as image, container, to perform the process.
#### Docker Registry:
A place docker images are stored. DockerHub is such a public registry.

### BASIC DOCKER COMMANDS
* To pull images from docker repository  
>$ docker pull (image name)
* To push the images to docker repository
>$ docker push (image name)
* To create container from image
>$ docker run -it-d (image name)
* To stop the running container
>$ docker stop (container id)
* To list the running containers
>$ docker ps
