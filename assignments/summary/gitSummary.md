# **GIT**
Git is a distributed version control system (VCS),for tracking changes in computer files, helps  establishing coordination between multiple developers,it facilitates with functions like who made what changes and when, it allows to revert back at anytime etc.  

### **CONCEPTS OF GIT**
* keeps track of code history
* takes "snapshots" of your files
* you decide when to take a snapshot by makng a "commit"
* you can stage files before commitng

### **GIT BASIC WORK FLOW**
Git has three stages in which files can reside in:
* Modified : changed the file but have not comitted to repo yet 
* Staged : marked a modified in its current version to go into next snapshot
* Committed : data is safely stored in local repo

#### **4 different trees** of software repository are present namely:
* Workspace: all changes we make via editors is done in this tree repository
* Staging:   all the staged file go into this tre of repository
* Local repository: all committed files go to this repository
* Remote repository: this is the copy of local repository but stored in some server on internet. All the changes made in local repositor are not   directly reflected into this tree , but we have to push the changes to the remote repository

![git workflow](https://tse1.mm.bing.net/th?id=OIP.YrxiN3hL4TRRaIPltbTDIAHaFL&pid=Api&P=0&w=250&h=176)

### GIT COMMANDS:
* Configuration: To configure git in machine.
>$ git config  --global user.name 'name-of -the-user'
>$ git config --global user.email 'email-id'
* Initialize: To create .git directory in machine.
>$ git init
* Clone: Cloning the remote repo to local repo.
>$ git clone (link-to-repo)
* Status: To check the status of files in staging area.
>$ git status 
* Add: To add the files to staging area.
>$ git add
* Push: To push the file from local repo to remote repo.
>$ git push origin     